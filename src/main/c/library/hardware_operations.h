#pragma once

#include "common.h"


extern char * RAM;
extern size_t fault_count;

struct page_table_entry {
  size_t linear_address;
  size_t physical_address;
  //unsigned int L;
  BOOL in_memory;
  BOOL in_use;
};

struct page_table {
  struct page_table_entry entries[16];
};

//extern struct page_table* active_page_table;

// background reading
// https://stackoverflow.com/questions/18431261/how-does-x86-paging-work

// these simulate assembly instructions, where data is equivelent to a register

void set_active_page_table(struct page_table* tbl); // cr3 reg on x86

ERROR store_long(size_t pointer, long data);
ERROR store_int(size_t pointer, int data);
ERROR store_short(size_t pointer, short data);
ERROR store_char(size_t pointer, char data);


ERROR load_long(size_t pointer, long* out);
ERROR load_int(size_t pointer, int* out);
ERROR load_short(size_t pointer, short* out);
ERROR load_char(size_t pointer, char* out);

