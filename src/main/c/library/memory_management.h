#pragma once

#include "common.h"

// you must implement these methods in memory_management.c:
ERROR my_alloc(size_t needed_memory, size_t* output_address);
ERROR my_realloc(size_t needed_memory, size_t* current_address);
ERROR my_free(size_t address);


