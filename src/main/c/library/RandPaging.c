#include "paging.h"

ERROR RAND_page_fault(size_t linear_addr){
    return -1;
}

ERROR RAND_allocate_page(){
    return -1;
}

const struct paging_operations RAND_paging_operations = {
    .page_fault     = &RAND_page_fault,
    .allocate_page  = &RAND_allocate_page,
};

