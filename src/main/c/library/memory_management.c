#include "memory_management.h"
#include "paging.h"

// use the normal alloc functions to store memory pool lookup table
// remember to check the value of `current` (Which processes memory am I accessing?)

ERROR my_alloc(size_t needed_memory, size_t* output_address){

    // if the user space program needs more space then
    // paging_strategy->allocate_page(); gives you PAGE_SIZE more bytes

    // here you have to give an address (*output_address = <something>)
    // how does free work?

    return -1;
}

ERROR my_realloc(size_t needed_memory, size_t* current_address){
    return -1;
}

ERROR my_free(size_t address){
    return -1;
}



