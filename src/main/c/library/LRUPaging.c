#include "paging.h"

ERROR LRU_page_fault(size_t linear_addr){
    return -1;
}

ERROR LRU_allocate_page(){
    return -1;
}

const struct paging_operations LRU_paging_operations = {
    .page_fault      = &LRU_page_fault,
    .allocate_page   = &LRU_allocate_page,
};

