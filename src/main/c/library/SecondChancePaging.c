#include "paging.h"

ERROR second_chance_page_fault(size_t linear_addr){
    return -1;
}

ERROR second_chance_allocate_page(){
    return -1;
}

const struct paging_operations second_chance_paging_operations = {
    .page_fault     = &second_chance_page_fault,
    .allocate_page  = &second_chance_allocate_page,
};

