#pragma once

// Do NOT modify any part of this file.

#include "common.h"

// page size is as small as possible for ease of testing
#define PAGE_SIZE 64 // bytes
#define RAM_SIZE (PAGE_SIZE * 8)
#define SWAP_SIZE (RAM_SIZE * 2)
#define SWAP_FILE "./swap.dat" 

#define FIFO_PAGING 0
#define LRU_PAGING 1
#define SECOND_CHANCE_PAGING 2
#define RANDOM_PAGING 3

struct paging_operations {
  ERROR (*page_fault)(size_t linear_addr);
  ERROR (*allocate_page)();
};

extern const struct paging_operations FIFO_paging_operations;
extern const struct paging_operations LRU_paging_operations;
extern const struct paging_operations second_chance_paging_operations;
extern const struct paging_operations RAND_paging_operations;

extern struct paging_operations *paging_strategy;
extern size_t current;// pid of the process "on the cpu"
extern char * RAM;

void set_strategy(size_t paging);
void boot(size_t paging);
void allocate_processor_to(size_t pid);

// return number of page faults observed
size_t number_of_page_faults();
