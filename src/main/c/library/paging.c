#include "paging.h"
#include "hardware_operations.h"

char * RAM = NULL;
struct paging_operations *paging_strategy = NULL;
size_t current;

void set_strategy(size_t paging){
    switch(paging){
        case FIFO_PAGING:           paging_strategy = (struct paging_operations *)&FIFO_paging_operations; break;
        case LRU_PAGING:            paging_strategy = (struct paging_operations *)&LRU_paging_operations;  break;
        // Bonus
        case SECOND_CHANCE_PAGING:  paging_strategy = NULL;//(struct paging_operations *)&second_chance_paging_operations;  break;
        case RANDOM_PAGING:         paging_strategy = NULL;//(struct paging_operations *)&RAND_paging_operations;  break;
    }

}

size_t number_of_page_faults(){
    return fault_count;
}


/* Helper function 
void increment_fault_count(){
    fault_count++;
}
*/

