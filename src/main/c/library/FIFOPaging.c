#include "paging.h"

ERROR FIFO_page_fault(size_t linear_addr){
    // if the page for addr is not in memory, 
    // then do we allocate a fresh frame (lazy allocation)
    // or do we have to copy from swap space?

    return -1;
}

ERROR FIFO_allocate_page(){
    // this is where you add a new page table entry

    return -1;
}

const struct paging_operations FIFO_paging_operations = {
    .page_fault      = &FIFO_page_fault,
    .allocate_page  = &FIFO_allocate_page,
};

