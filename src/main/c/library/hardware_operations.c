#include "common.h"
#include "hardware_operations.h"
#include "paging.h"
#include <unistd.h>

size_t fault_count = 0;
struct page_table* active_page_table = NULL;

/* Few helper routines that a student can choose to use */




void set_active_page_table(struct page_table* tbl){
    active_page_table = tbl;
}

ERROR address_of(size_t pointer, void **ptr) {
    // six offset bits for a 64 byte page size
    size_t page_address = pointer & (~0x2F);
    size_t offset = pointer & 0x2F;
    struct page_table_entry* entry = NULL;
    unsigned short i;

    for(i=0; i<16; i++){
        if(active_page_table->entries[i].linear_address == page_address && entry->in_use == TRUE) {
            entry = &active_page_table->entries[i];
            break;
        }
    }
    if(entry == NULL){
        printf("\"SEG FAULT\"");
        return -1;
    } else {
        if(entry->in_memory == FALSE){
            fault_count++;
            paging_strategy->page_fault(pointer);
        }
        // page should now be in memory, tables physical address should be accurate
        *ptr = &RAM[entry->physical_address + offset];
        return 0;
    };
}

#define store(type) \
    ERROR store_ ##type (size_t pointer, type data){\
        type* ptr;\
        if(address_of(pointer, (void**)&ptr) != 0){\
            return -1;\
        } else {\
            *ptr = data;\
            return 0;\
        }\
    }

store (long)
store (int)
store (short)
store (char)

#define load(type) \
    ERROR load_ ## type (size_t pointer, type* data){\
        if(address_of(pointer, (void**)&data) != 0){\
            return -1;\
        } else {\
            return 0;\
        }\
    }

load(long)
load(int)
load(short)
load(char)
