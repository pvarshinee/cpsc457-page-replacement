#include "library/paging.h"
#include "library/memory_management.h"
#include "library/hardware_operations.h"

// this is a example of how I will test your assignment
// all code in main will be replaced during grading

// However you should extend the code here for your own testing purposes.

void fail(const char* msg) {
  printf("%s", msg);
  exit(-1);
}

void assert(const char* method, long expected, long actual) {
  printf("%s: expected [ %l ], actual [ %l ]", method, expected, actual);
}

int test(){
  size_t array_len;
  size_t array;
  long i;
  long value;
  int error;

  /* -------------- Initialization of simulated environment ------------- */
  // set paging strategy
  boot(FIFO_PAGING);  
  // assuming that 1 is the current process in our simulated machine
  allocate_processor_to(1); 

  // allocate some units of memory 'm' to the current process
  array_len = SWAP_SIZE / sizeof(long);
  if(my_alloc(SWAP_SIZE, &array) != 0){
    fail("test failed on my_alloc");
  }

  /* -------------------------------------------------------------------- */

  // now try accessing m by writing data to it.
  // this is supposed to create page fault when the page is accessed first time
  for(i=0; i<array_len; i++) {
    if(store_long(array + (i * sizeof(long)), i) != 0){
      fail("test failed on store");
    }
  }

  // check that every value of the array is what we set it to.
  for(i=0; i<array_len; i++) {
    // read the value at array[i] into a variable called value
    // note that array[i] = array + (i * sizeof(long))
    // sizeof(long) because elements stored in the array are of type long
    if(load_long(array + (i * sizeof(long)), &value) != 0){
      fail("test failed on load");
    }
    assert("test", i, value);
  }

  // check if actual number of page faults is equal to expected
  // number of page faults
  assert("test", 16, number_of_page_faults());

  // free the allocated memory
  if(my_free(array) != 0){
    fail("test failed on my_free");
  }

  return 0;
}

/* TEST CASES

allocate 1 peice of memory
fill memory & some of swap space
fill memory & 1 page, repeatedly ask for page expected outside of memory
fill memory & swap space & ask for more memory


 */

int main() {
  test();
}
